package org.databox;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import blackboard.db.BbDatabase;
import blackboard.persist.DatabaseContainer;
import blackboard.platform.context.Context;

public class registroClicks  {

	
	private String id;
	private String course;
	private String link;
	private Context ctx;
	
	public registroClicks(Context bbContext) throws Exception {
		DatabaseContainer.getDefaultInstance().getBbDatabase().getConnectionManager().setTraceConnectionOpeners(true);
		this.ctx = bbContext;
		this.setCourse(this.ctx.getCourse().getCourseId());
		
			
	}
	
	public String imgBiblioURL()throws Exception {
		String imgURL="";
		
		
		String[] parts = this.getCourse().split("-");
		int hayBiblio=0;
		Connection conn = null;
		conn = BbDatabase.getDefaultInstance().getConnectionManager().getConnection();
		for(int i=0;i<parts.length;i++)
		{
			// OPEN DB CONNECTION
			
			
			
			String queryPostgre="SELECT link from lnoh_biblioteca_link where course = '"+parts[i]+"'";
			String queryOracle="";
			ResultSet rSetPost = conn.prepareStatement(queryPostgre).executeQuery();
			//ResultSet rSetOracle = conn.prepareStatement(queryOracle).executeQuery();
			if (rSetPost.next()) {
				imgURL="<a id=\"linkBiblio\"  href=\""+rSetPost.getString("link")+"\" target=\"_blank\">"+"<img id=\"imgBiblio\" src=\"Resources/iconoBiblio.png\" onClick=\"insertTheClick\"></img></a>";
				hayBiblio=1;
			}
			
			rSetPost.close();
			
			
		}
		
		
		if(hayBiblio==0)
		{
			imgURL="<img id=\"imgBiblio\" src=\"Resources/BibliografiaNoDisponible.png\"></img>";  
		}
		else
		{
			/*PreparedStatement ps = conn.prepareStatement("INSERT INTO blockBiblioteca VALUES(lnoh_block_biblioteca_SEQ.nextval,?,?,?,?,?)");
			long unixTime = System.currentTimeMillis() / 1000L;
			String[] values = {ctx.getCourseId().toString(),ctx.getUserId().toString(),String.valueOf(unixTime) ,ctx.getCourseMembership().getRoleAsString(),};
			this.setParameters(values, ps);
			ps.executeBatch();
			ps.clearBatch();
			ps.close();
			ps = null;*/
		}
		
		conn.close();
		conn = null;
		
		//<img id="imgBiblio" src="Resources/iconoBiblio.png"></img>
		// CLOSE DB CONNECTION
		
		
		return imgURL;
	}
	
	public String testTheClick()throws Exception{
		Connection conn = null;
		conn = BbDatabase.getDefaultInstance().getConnectionManager().getConnection();
		long unixTime = System.currentTimeMillis() / 1000L;
		String[] parts = this.getCourse().split("-");
		String queryPostgre="INSERT INTO lnoh_block_biblioteca(id,courseid,userid,fecha,rolid,modulo) VALUES(1,'"+ctx.getCourse().getCourseId();//+"','"+ctx.getUserId().toString()+"','"+String.valueOf(unixTime)+"','"+ctx.getCourseMembership().getRoleAsString()+"','"+parts[0]+"')";
		/*ResultSet rSetPost = conn.prepareStatement(queryPostgre).executeQuery();
		rSetPost.close();*/
		return "<p>VOLCADO HORARIO DOCENTE</p>";
	}
	
	public void dbTheClick()throws Exception{
		Connection conn = null;
		conn = BbDatabase.getDefaultInstance().getConnectionManager().getConnection();
		long unixTime = System.currentTimeMillis() / 1000L;
		String[] parts = this.getCourse().split("-");
		//CourseMembership cm = CourseMembershipDbLoader.Default.getInstance().loadByCourseAndUserId(ctx.getCourseId(), ctx.getUserId());
		//BbDatabase.getDefaultInstance()
		Integer idIncr=1;
		String queryIns="INSERT INTO lnoh_block_biblioteca(id,courseid,userid,fecha,rolid,modulo) VALUES(lnoh_block_biblioteca_SEQ.nextval,'"+ctx.getCourse().getCourseId()+"','"+ctx.getUser().getStudentId()+"',"+String.valueOf(unixTime)+","+"3"+","+"'"+parts[0]+"')";
		idIncr=idIncr+1;
		PreparedStatement ps = conn.prepareStatement(queryIns);//"INSERT INTO lnoh_block_biblioteca VALUES(lnoh_block_biblioteca_SEQ.nextval,?,?,?,?,?)");
		System.out.println(queryIns);
		ps.executeQuery();
		//ps.clearBatch();
		ps.close();
		ps = null;
		conn.close();
		conn = null;
		//String[] values = {ctx.getCourse().getCourseId(),ctx.getUserId().toString(),String.valueOf(unixTime) ,ctx.getCourseMembership().getRoleAsString(),""};
		//this.setParameters(values, ps);
		
		
	}
	
	public String showUploadBiblioButton()throws Exception {
		String imgURL="";
		imgURL="<input type=\"image\" src=\"Resources/dbup.png\" width=\"30\" height=\"30\" title=\"Clic para actualizar bibliografia.\">";
		return imgURL;
	}
	
	public String validateStartDataUploadProgress()
	{
		String returnValue="";
		
		return returnValue;
	}
	
	
	
	public String getlink(){
		return link;
	}
	
	public void setlink(String link)
	{
		this.link=link;
	}
	
	public String getCourse(){
		return course;
	}
	
	public void setCourse(String course){
		this.course=course;
	}
	
	public void setParameters(String[] array, PreparedStatement ps) throws Exception 
	{
		int i = 1;
		ps.setInt(i++, Integer.valueOf(array[0]));
		ps.setInt(i++, Integer.valueOf(array[1])); 
		ps.setInt(i++, Integer.valueOf(array[2]));
		ps.setInt(i++, Integer.valueOf(array[3]));
		ps.setString(i++, array[4]);
		ps.addBatch();
	}
	

	
}