<!DOCTYPE html> 
	<%@ page language="java" contentType="text/html;UTF-8" pageEncoding="UTF-8" %>   
    <%@ page import="org.databox.DataHandler" %>
    <%@ page import="blackboard.data.user.User.SystemRole" %>
	<%@ page import="blackboard.data.course.CourseMembership"%>
	<%@ page import="blackboard.persist.course.CourseMembershipDbLoader"%>
	<%@taglib uri="/bbData" prefix="bbData" %>
	<%@taglib uri="/bbNG" prefix="bbNG" %>
	 <%@ page import="org.databox.subidaData" %>
	

<bbData:context id="ctx">
<head>

<style>
	
</style>
</head>
<body>
	<bbNG:genericPage title="Cargando Bibliografia">
		<bbNG:pageHeader>
        <bbNG:breadcrumbBar environment="SYS_ADMIN">
            <bbNG:breadcrumb title="Cargando Bibliografia"/>
        </bbNG:breadcrumbBar>
        <bbNG:pageTitleBar title="Cargando" showIcon="true"
                           showTitleBar="true"/>
                           
    </bbNG:pageHeader>
    <bbNG:dataCollection>
    	<bbNG:step title="Subir archivo plano de la Bibliografia">
    		<bbNG:dataElement label="Detalle de subida de datos"
                                  isRequired="false" >
                <textarea rows="30" cols="200"  readonly>
                	<% 
    					subidaData subida = new subidaData();
						out.print(subida.subidaData(request)); 
					%>
                </textarea>
				
           </bbNG:dataElement>
    		
                                  
                                  
            
    	</bbNG:step>
    	<bbNG:stepSubmit showCancelButton="true">
			 
        </bbNG:stepSubmit>
       
            
    </bbNG:dataCollection>
            
	</bbNG:genericPage>
	
	<!--  <div id="divUploadBiblio">
		<form method="POST" action="upload" enctype="multipart/form-data" >
            File:
            <input type="file" name="file" id="file" /> <br/>
            Destination:
            <input type="text" value="" name="destination"/>
            </br>
            
          </form>
	</div> -->
	
</body>
</bbData:context>