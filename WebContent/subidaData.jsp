<!DOCTYPE html> 
	<%@ page language="java" contentType="text/html;UTF-8" pageEncoding="UTF-8" %>   
    <%@ page import="org.databox.DataHandler" %>
    <%@ page import="blackboard.data.user.User.SystemRole" %>
	<%@ page import="blackboard.data.course.CourseMembership"%>
	<%@ page import="blackboard.persist.course.CourseMembershipDbLoader"%>
	<%@ page import="org.apache.poi.hssf.usermodel.HSSFSheet"  %>
	<%@ page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"  %>	
	<%@taglib uri="/bbData" prefix="bbData" %>
	<%@taglib uri="/bbNG" prefix="bbNG" %>
	

<bbData:context id="ctx">
<head>

<style>
	
</style>
</head>
<body>
	<bbNG:genericPage title="Carga de datos de Bibliografia">
		<bbNG:pageHeader>
        <bbNG:breadcrumbBar environment="SYS_ADMIN">
            <bbNG:breadcrumb title="Bibliografia de Cursos"/>
        </bbNG:breadcrumbBar>
        <bbNG:pageTitleBar title="Carga de Datos de la Bibliografia de Cursos" showIcon="true"
                           showTitleBar="true"/>
                           
    </bbNG:pageHeader>
    <bbNG:dataCollection>
    	<bbNG:step title="Subir archivo plano de la Bibliografia">
    		<bbNG:dataElement label="Seleccionar archivo plano"
                                  isRequired="false" >
                                  <form id="formSubida" method="POST" action="/webapps/LNOH-AIEPBIBLIO-BBLEARN/subidaProceso.jsp" enctype="multipart/form-data" >
                                  		<input type="file" name="inputFile" id="inputFile"  value="Examinar" name="subidaBiblio">
                                  		<input type="hidden" id="fileName" name="fileName" >
                                  		
                                  </form>
                                  
                                  
            </bbNG:dataElement>
    	</bbNG:step>
    	<bbNG:stepSubmit showCancelButton="true">
			 <bbNG:stepSubmitButton label="Actualizar"  onClick="validateSubida()"/>
        </bbNG:stepSubmit>
       
            
    </bbNG:dataCollection>
            
	</bbNG:genericPage>
	
	<!--  <div id="divUploadBiblio">
		<form method="POST" action="upload" enctype="multipart/form-data" >
            File:
            <input type="file" name="file" id="file" /> <br/>
            Destination:
            <input type="text" value="" name="destination"/>
            </br>
            
          </form>
	</div> -->
	<script type="text/javascript">
		function validateSubida()
		{
			var botonSubida=document.getElementById("inputFile");
			
			if(botonSubida.files.length==0)
				{
					window.alert("Elegir un archivo para actualizar la bibliografia");
				}
			else
				{
					if(botonSubida.files.length==1)
						{
						//document.getElementById("fileName").value=document.getElementById("inputFile").value;
						document.getElementById("formSubida").submit();
							
							
						}
					else
						{
							window.alert("Seleccionar solo un archivo");
						}
							
				}
		}
	</script>
</body>
</bbData:context>